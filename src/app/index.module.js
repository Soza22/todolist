(function() {
    'use strict';

    angular
        .module('angularSeedApp', ['ngResource', 'ui.router']);

})();